package pe.gob.general.microservice;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class MicroserviceErrorHandler
{
    @ExceptionHandler(NoHandlerFoundException.class)
    String GeneralError404(HttpServletRequest request, Exception e)
    {
        return "--404--";
    }

}

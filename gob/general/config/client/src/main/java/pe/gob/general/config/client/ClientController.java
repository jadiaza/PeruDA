package pe.gob.general.config.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RefreshScope
public class ClientController
{
    @Value("${app.description}")
    String appDescription;

    @Value("${my.greeting}")
    String greeting;

    @Value("${my.list.values}")
    List<String> Values;

    @GetMapping("/test")
    String Test()
    {
        return "Greeting: " + greeting + "<br>Values: " + Values + "<br>Description: " + appDescription;
    }

    @PostMapping("/test2")
    ResponseEntity<Map<String, Object>> testPost(@RequestBody Map<String, Object> body) {
      Map<String, Object> resp = new HashMap<>();
      resp.put("greeting", greeting);
      resp.putAll(body);
      return new ResponseEntity<>(resp, HttpStatus.ACCEPTED);
    }
}
